#pragma once

#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <sys/socket.h>
using std::string;

namespace wd
{

class InetAddress
{
  public:
    explicit InetAddress(unsigned short);        // port
    InetAddress(const string &, unsigned short); // ip port
    InetAddress(const struct sockaddr_in);
    string ip() const;
    unsigned short port() const;
    struct sockaddr_in *getAddrPtr();

  private:
    struct sockaddr_in _addr; // 网络字节序
};

} // end of namespace wd