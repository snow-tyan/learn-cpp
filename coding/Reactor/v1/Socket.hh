#pragma once

namespace wd
{

class Socket
{
  public:
    // RAII托管套接字，构造时创建套接字，析构时释放套接字
    Socket();
    explicit Socket(int);
    ~Socket();

    int fd();
    void shutdownWrite(); // 主动断开连接 关闭写端

  private:
    int _fd;
};

} // end of namespace wd