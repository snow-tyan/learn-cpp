#pragma once

#include <functional>
using std::function;

namespace wd
{

using TimerCallback = function<void()>;

class Timer
{
  public:
    Timer(int, int, TimerCallback &&);
    ~Timer();
    void start();
    void stop();

  private:
    int createTimerFd();
    int getTimer();
    void setTimerFd(int, int); // 设置定时器，setTimerFd(0, 0)表示停止定时器
    void handleRead();

  private:
    int _timerfd;
    TimerCallback _cb;
    int _initialTime;  // 初始化时间
    int _periodicTime; // 周期时间
    bool _isStarted;
};

} // end of namespace wd