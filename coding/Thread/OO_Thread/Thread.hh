#pragma once
#include "NonCopyable.hh"
#include <pthread.h>

namespace wd
{

class Thread
    : public Noncopyable
{
  public:
    Thread() : _pthid(0), _isRunning(false){};
    virtual ~Thread();
    void start(); // 创建线程
    void join();  // 等待回收
    // 线程入口函数形式固定，设置成static干掉this指针
    static void *threadFunc(void *);

  private:
    virtual void run() = 0; // 抽象任务

  private:
    pthread_t _pthid;
    bool _isRunning;
};

} // end of namespace wd