#include <iostream>
using namespace std;

class A
{
  public:
    void func()
    {
        cout << "A::func()" << endl;
    }

  private:
    int _a;
};

class B
{
  public:
    void func()
    {
        cout << "B::func()" << endl;
    }

  private:
    int _b;
};

class C
    : public A,
      public B
{
  private:
    int _c;
};

int main()
{
    C *c = new C();
    c->A::func();
    c->B::func();
    cout << "sizeof(A)=" << sizeof(A) << endl;
    cout << "sizeof(B)=" << sizeof(B) << endl;
    cout << "sizeof(C)=" << sizeof(C) << endl;
    return 0;
}