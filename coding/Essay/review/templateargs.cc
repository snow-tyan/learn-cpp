#include <iostream>
using namespace std;

template <class T>
void print(T t)
{
    cout << t << endl;
}

template <class T, class... Args>
void print(T t, Args... args)
{
    cout << t << " ";
    print(args...);
}

int main()
{
    int a = 1;
    int b = 2;
    print(a, b);
    int c[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    return 0;
}